const mongoose =  require('./database');

const licenseSchema = new mongoose.Schema ({
    initial_date: {
        type: String,
        required: true
    },
    final_date: {
        type: String,
        required: true
    },
    time: {
        type: Number,
        required: true
    },
    member_code: {
        type: Number,
        required: true
    },
});

const license = mongoose.model('license', licenseSchema);

module.exports = license;