const express = require('express');

const Atestado = require('./medical_license');

const router = express.Router();

router.post('/register', async (req, res) => {
    try {
        const atestado = await Atestado.create(req.body);

        return res.send({ atestado });
      } catch (err) {
        return res.status(400).send({ error: 'Registration failed' });
    
    }
});

module.exports = app => app.use('/atestado', router);