const express = require('express');
const app = express();
const employer = require("./employer.json");
const member = require("./member.json")

app.use(express.json());

//----------------------------------------------------------------------------
app.get("/Empregador", function(req, res) {
    res.json(employer);
});

app.get("/Empregador/:employer_code", function(req, res) {
    const { employer_code } = req.params;
    const client = employer.find(cli => cli.employer_code == employer_code);

    if(!client) return res.status(404).json();

    res.json(client);
});

//----------------------------------------------------------------------------

app.get("/Membro", function(req, res) {
    res.json(member);
});

app.get("/Membro/:pin_code", function(req, res) {
    const { pin_code } = req.params;
    const client = member.find(cli => cli.pin_code == pin_code);

    if(!client) return res.status(404).json();

    res.json(client);
});

//----------------------------------------------------------------------------

require('./authController')(app);

app.listen(3000, function() {
    console.log("Server is running");
});